﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            var counter = 6;
            var i = 0;

            for (i = 0; i < counter; i++)
            {
                var a = i + 1;
                Console.WriteLine($"This is line number {a}");
            }
        }
    }
}
